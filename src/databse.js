var mongoose = require('mongoose');

module.exports.connectDatabase = async () => {

    await mongoose.connect(' mongodb://127.0.0.1:27017/test',  { 
                useUnifiedTopology: true,
                useNewUrlParser: true,
                useFindAndModify: false
    })
    console.log('Database conected') 
}