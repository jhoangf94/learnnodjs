const UserModel = require('../models/user')
const bcrypt = require('bcryptjs')

module.exports.create_user = async function(req, res) {

    var user = {
        ...req.body
    }

    try {
        // encriptar el password
        const encryptePassword = await bcrypt.hash(req.body.password, 10)
        user.password = encryptePassword

        const data = await UserModel.create(user)
        res.send(data)
    }catch(err){
        res.send(err)
    }
}

module.exports.get_all_users = function(req, res) {
    const users = UserModel.find((err, users) => {
        if (err){
            res.status(404).send({error: err})
        } else {
            res.send(users)
        }
    })
}

module.exports.find_user = async function(req, res) {
    
    try {
        const id = req.params.id 

        const users = await UserModel.findById(id)

        if(!users){
            res.status(404).send('No encontre nada')
        }else{
            res.send(users)
        }

    }catch(err){
        console.log(err)
        res.status(500).send({message: err})
    }

}