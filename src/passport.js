const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy
const JwtStrategy = require('passport-jwt').Strategy
const ExtractJwt = require('passport-jwt').ExtractJwt
const UserModel = require('./models/user')


// options for jwt validation
var opts = {}
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken()
opts.secretOrKey = 'mySecretKey'

passport.use(
    new JwtStrategy(opts, async (jwt_payload, done) =>  {

        try {
            
            const user = await UserModel.findOne({ email: jwt_payload.email })
            const passwordCorrect = user.password == jwt_payload.password ? true : false

            if (user && passwordCorrect) {
                return done(null, user);
            } else {
                return done(null, false);
            }

        } catch (err) {
            return done(err, false)
        }
    })
)

// Estrategia local para realizar login
passport.use( new LocalStrategy(
    {
        usernameField: 'email',
        passwordField:'password',
    },
    async (email, password, callback) => {

        try {
            const user = await UserModel.findOne({ email})

            if (!user){
                return callback(null, false, {message: "Incorrect email or password"})  
            }

            return callback(null, user, {message: "Logged in successfully"})

        } catch (err) {
            callback(err)
        }
    } )
)

module.exports = passport
