// Utilizar funcionalidades del Ecmascript 6
'use strict'

// Cargamos el módulo de mongoose para poder conectarnos a MongoDB


// *Cargamos el fichero app.js con la configuración de Express
var app = require('./app');

const conn = require('./databse')

// Creamos la variable PORT para indicar el puerto en el que va a funcionar el servidor
app.set('port',3000)

// Le indicamos a Mongoose que haremos la conexión con Promesas

const main = async() => {
    try {
        conn.connectDatabase()
        app.listen(app.get('port'))
        console.log('App Listen: ', app.get('port'))

    }catch(e){
        console.log(e)
    }
}

main()
