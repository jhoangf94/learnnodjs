'use strict'
// Cargamos los módulos de express y body-parser

const express = require('express');
const passport = require('./passport')
const bodyParser = require('body-parser');
const morgan = require('morgan')
const user_routes = require('./routes/user'); 
const auth_routes = require('./routes/auth'); 

// Llamamos a express para poder crear el servidor
var app = express();

app.use(bodyParser.urlencoded({extended:false}));

app.use(bodyParser.json());

app.use(morgan('dev'))

app.use(passport.initialize())

// Cargamos las rutas
app.use('/', user_routes);
app.use('/', auth_routes)

// exportamos este módulo para poder usar la variable app fuera de este archivo
module.exports = app;