const router = require('express').Router()
const authController = require('../controllers/auth')
const passport = require('../passport')

router.post('/login', passport.authenticate('local', {session: false}) , authController.auth)
router.get('/profile', passport.authenticate('jwt', {session: false}) , authController.profile)

module.exports = router