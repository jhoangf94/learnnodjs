const routes = require('express').Router()
const usersController = require('../controllers/users')

routes.post('/users', usersController.create_user)
routes.get('/users', usersController.get_all_users)
routes.get('/users/:id', usersController.find_user)

module.exports = routes 